import { NgModule } from '@angular/core';
import { CuentaRoutingModule } from './cuenta-routing.module';
import { MainCuentaComponent } from './main-cuenta/main-cuenta.component';
import { ClientEditComponent } from './client-edit/client-edit.component';
import { AcountEditComponent } from './acount-edit/acount-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    MainCuentaComponent,
    ClientEditComponent,
    AcountEditComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CuentaRoutingModule
  ]
})
export class CuentaModule {}
