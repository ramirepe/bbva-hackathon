import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public clientObject: any = {
    dni: '',
    password: ''
  };

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private db: AngularFirestore
  ) {
    const tutorialsRef = db.collection('tutorials');
    const tutorial = { title: 'zkoder Tutorial', url: 'bezkoder.com/zkoder-tutorial' };
    tutorialsRef.add(this.clientObject).then((r) =>{
      this.router.navigate(['dashboard-client']);
    }).catch((err) => {
      console.log(err);
    });
  }

  ngOnInit(): void {

  }

  login() {
    console.log(this.clientObject);
  }

  register() {
    this.router.navigate(['register']);
  }
}
